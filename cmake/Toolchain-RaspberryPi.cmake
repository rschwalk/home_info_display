SET(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(CMAKE_SYSROOT /home/rschwalk/x-tools/armv8-rpi3-linux-gnueabihf/armv8-rpi3-linux-gnueabihf/sysroot)
set(CMAKE_STAGING_PREFIX /home/rschwalk/x-tools/rpi_sysroot)

set(tools /home/rschwalk/x-tools/armv8-rpi3-linux-gnueabihf)
set(CMAKE_C_COMPILER ${tools}/bin/armv8-rpi3-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER ${tools}/bin/armv8-rpi3-linux-gnueabihf-g++)

set(CMAKE_FIND_ROOT_PATH ${CMAKE_FIND_ROOT_PATH} /home/rschwalk/x-tools/rpi_sysroot)

link_directories(/home/rschwalk/x-tools/rpi_sysroot/opt/vc/lib)

#set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
