//
// Created by rschwalk on 5/10/20.
//

#include "tcp_server.hpp"

#include <boost/bind.hpp>

TCP_Server::TCP_Server(boost::asio::io_context &io_context,
                       std::mutex &mut,
                       std::queue<HID::Commands> &queue)
    : io_context_(io_context),
      acceptor_(io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), 4040)),
      mut_(mut),
      command_queue_(queue)
{
    start_accept();
}
void TCP_Server::start_accept()
{
    TCP_Connection::pointer new_connection = TCP_Connection::create(io_context_, mut_, command_queue_);
    acceptor_.async_accept(new_connection->socket(),
                           boost::bind(&TCP_Server::handle_accept,
                                       this,
                                       new_connection,
                                       boost::asio::placeholders::error));
}
void TCP_Server::handle_accept(const TCP_Connection::pointer &new_connection,
                               const boost::system::error_code &error)
{
    if (!error)
    {
        new_connection->start();
    }

    start_accept();
}
