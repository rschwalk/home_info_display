/******************************************************************************
 * File:            natworking_types.hppp
 *
 * Author:           rschwalk
 * Created:          05/15/20
 * Description:      The types to using in our networking layer.
 *****************************************************************************/

#ifndef HID_NETWORKING_TYPES
#define HID_NETWORKING_TYPES

namespace HID
{
    /*! \enum Commands
    *
    *  The command state, received over network.
    */
    enum Commands
    {
        CMD_RESTART,
        CMD_DONE,
        
        CMD_INVALID,
    };
} // namespace HID

#endif // !HID_NETWORKING_TYPES
