//
// Created by rschwalk on 5/10/20.
//

#include "tcp_connection.hpp"
#include <boost/bind.hpp>
#include <iostream>
#include <string>

TCP_Connection::TCP_Connection(boost::asio::io_context &io_context,
                               std::mutex &mut,
                               std::queue<HID::Commands> &queue)
    : socket_(io_context),
      mut_(mut),
      command_queue_(queue)
{
}

void TCP_Connection::handle_write(const boost::system::error_code &, size_t)
{
}

TCP_Connection::pointer TCP_Connection::create(boost::asio::io_context &io_context,
                                               std::mutex &mut,
                                               std::queue<HID::Commands> &queue)
{
    return TCP_Connection::pointer(new TCP_Connection(io_context, mut, queue));
}

void TCP_Connection::start()
{
    message_ = "Hello\n";

    socket_.async_read_some(boost::asio::buffer(buffer_), boost::bind(&TCP_Connection::handle_read,
                                                                      shared_from_this(),
                                                                      boost::asio::placeholders::error,
                                                                      boost::asio::placeholders::bytes_transferred));

    //    boost::asio::async_read(socket_, boost::asio::buffer(buffer_),
    //                            boost::bind(&TCP_Connection::handle_read,
    //                                shared_from_this(),
    //                                boost::asio::placeholders::error,
    //                                boost::asio::placeholders::bytes_transferred));

    boost::asio::async_write(socket_,
                             boost::asio::buffer(message_),
                             boost::bind(&TCP_Connection::handle_write,
                                         shared_from_this(),
                                         boost::asio::placeholders::error,
                                         boost::asio::placeholders::bytes_transferred));
}

void TCP_Connection::handle_read(const boost::system::error_code &error, size_t bytes_transferred)
{
    if (!error) {
        std::string command(buffer_.data(), bytes_transferred);

        auto cmd = HID::CMD_INVALID;
        if (command == "DONE\r\n" || command == "DONE") {
            cmd = HID::CMD_DONE;
        }
        else if (command == "RESTART\r\n" || command == "RESTART") {
            cmd = HID::CMD_RESTART;
        }

        message_ = "Command received!";

        std::lock_guard<std::mutex> guard(mut_);
        command_queue_.push(cmd);

        std::cout << "Received: " << command << std::endl;

        boost::asio::async_write(socket_,
                                 boost::asio::buffer(message_),
                                 boost::bind(&TCP_Connection::handle_write,
                                             shared_from_this(),
                                             boost::asio::placeholders::error,
                                             boost::asio::placeholders::bytes_transferred));
    }
}
