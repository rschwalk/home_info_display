#include <boost/asio.hpp>
#include <chrono>
#include <iostream>
#include <queue>
#include <thread>
#include <SDL.h>

#include "natworking_types.hpp"
#include "tcp_server.hpp"
#include "sdl_cleanup.hpp"

int main()
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
        return 1;
    }

    SDL_Window *win = SDL_CreateWindow("Hello SDL",
                                       SDL_WINDOWPOS_CENTERED,
                                       SDL_WINDOWPOS_CENTERED,
                                       800,
                                       600,
                                       SDL_WINDOW_SHOWN);
    if (win == nullptr) {
        std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
        SDL_Quit();
        return 1;
    }

    SDL_Renderer *renderer =
        SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == nullptr) {
        std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
        cleanup(win);
        SDL_Quit();
        return 1;
    }

    SDL_SetRenderDrawColor(renderer, 195, 217, 255, 255);


    try {
//        using namespace std::chrono_literals;
        std::mutex mut;
        std::queue<HID::Commands> command_queue;
        boost::asio::io_context io_context;
        const TCP_Server server(io_context, mut, command_queue);
        std::thread tcp_thread{[&io_context]()
                               { io_context.run(); }};

        HID::Commands cmd;
        auto running = true;
        SDL_Event e;
        while (running) {
            while (SDL_PollEvent(&e)) {
                if (e.type == SDL_QUIT) {
                    running = false;
                }
                if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE) {
                    running = false;
                }
                if (e.type == SDL_WINDOWEVENT && e.window.type == SDL_WINDOWEVENT_CLOSE) {
                    running = false;
                }
            }

            SDL_RenderClear(renderer);
            SDL_RenderPresent(renderer);

            if (command_queue.empty()) {
                std::this_thread::sleep_for(std::chrono::seconds(1));
            }
            else {
                {
                    std::lock_guard<std::mutex> pop_guard(mut);
                    cmd = command_queue.front();
                    command_queue.pop();
                }
                switch (cmd) {
                case HID::CMD_DONE:running = false;
                    std::cout << "Command: " << cmd << std::endl;
                    break;

                case HID::CMD_RESTART:std::cout << "The system will restart..." << std::endl;
                    break;

                default:std::cout << "Not a valid Command: " << cmd << std::endl;
                    break;
                }
            }
        }

        io_context.stop();

        tcp_thread.join();
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    cleanup(renderer, win);
    SDL_Quit();
    std::cout << "The End!" << std::endl;

    return 0;
}
