//
// Created by rschwalk on 5/10/20.
//

#ifndef _TCP_CONNECTION_HPP_
#define _TCP_CONNECTION_HPP_

#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/array.hpp>
#include <string>
#include <thread>
#include <queue>

#include "natworking_types.hpp"

class TCP_Connection : public boost::enable_shared_from_this<TCP_Connection>
{
public:
    typedef boost::shared_ptr<TCP_Connection> pointer;

    static pointer create(boost::asio::io_context &io_context,
                          std::mutex &mut,
                          std::queue<HID::Commands> &queue);

    boost::asio::ip::tcp::socket &socket() { return socket_; }

    void start();

    virtual ~TCP_Connection() = default;

private:
    explicit TCP_Connection(boost::asio::io_context &io_context, std::mutex &mut, std::queue<HID::Commands> &queue);
    void handle_write(const boost::system::error_code & /*error*/, size_t /*bytes_transferred*/);
    void handle_read(const boost::system::error_code &error, size_t bytes_transferred);

    boost::asio::ip::tcp::socket socket_;
    std::string message_;
    boost::array<char, 1024> buffer_;
    std::mutex &mut_;
    std::queue<HID::Commands> &command_queue_;
};

#endif //_TCP_CONNECTION_HPP_
