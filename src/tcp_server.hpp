//
// Created by rschwalk on 5/10/20.
//

#ifndef _TCP_SERVER_HPP_
#define _TCP_SERVER_HPP_

#include <boost/asio.hpp>
#include <thread>
#include <queue>

#include "tcp_connection.hpp"
#include "natworking_types.hpp"

class TCP_Server
{
public:
    explicit TCP_Server(boost::asio::io_context &io_context,
                        std::mutex &mut,
                        std::queue<HID::Commands> &queue);

    virtual ~TCP_Server() = default;

private:
    void start_accept();
    void handle_accept(const TCP_Connection::pointer &new_connection, const boost::system::error_code &error);

    boost::asio::io_context &io_context_;
    boost::asio::ip::tcp::acceptor acceptor_;
    std::mutex &mut_;
    std::queue<HID::Commands> &command_queue_;
};

#endif //_TCP_SERVER_HPP_
